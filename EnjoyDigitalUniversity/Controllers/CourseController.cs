﻿using System.Web.Mvc;
using EnjoyDigitalUniversity.Models;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using System.Web;
using EnjoyDigitalUniversity.ViewModels;

namespace EnjoyDigitalUniversity.Controllers
{
    public class CourseController : SurfaceController
    {
        public CourseController()
        {
        }

        public PartialViewResult Listing(string department)
        {
            var model = new CoursesViewModel();

            //Hint get current pages children, map to list of courses and populate CourseViewModel
            //var courses = CurrentPage.

            return PartialView(model);
        }

        public PartialViewResult Detail()
        {
            var model = Map(CurrentPage);

            return PartialView(model);
        }

        public PartialViewResult ApplyForm()
        {
            var student = new Student
            {
                CourseId = CurrentPage.Id
            };

            return PartialView(student);
        }

        public ActionResult Apply(Student student)
        {            
            return CurrentUmbracoPage();
        }

        public CourseViewModel Map(IPublishedContent content)
        {
            var course = new CourseViewModel();

            course.Id = content.Id;
            course.BodyText = content.GetPropertyValue<IHtmlString>("bodyText");
            return course;
        }
    }
}